/*
    Autor: Simon Podlesny (xpodle01)

    
    Popis: 
        Program implementuje vypocet prirodzeneho logaritmu iba pomocou matematickych operacii +,-,*,/ 
        Zaroven program vyhlada pozadovany pocet iteracii pre pozadovanu presnost logaritmu z hodnot zadaneho intervalu
*/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>

const char ERROR_M [] = "Nevhodny pocet alebo format zapisu!\n";
const char HELP_M [] = 
    "Autor:\n" 
        "\tSimon Podlesny (xpodle01)\n"
    "Popis:\n" 
        "\tProgram implementuje vypocet prirodzeneho logaritmu iba pomocou matematickych operacii +,-,*,/\n"
        "\tZaroven program vyhlada pozadovany pocet iteracii pre pozadovanu presnost logaritmu z hodnot zadaneho intervalu\n"
    "Pouzitie:\n"
        "\tVypocet logaritmu:\n"
            "\t\t--log hodnota pocet_iteracii\n"
        "\tVypocet poctu iteracii:\n"
            "\t\t--iter min_hodnota max_hodnota presnost\n";


/*
Funkcia vypocita logaritmus cez dva implementacie tayloroveho polynomu

    Vstup:
        (double) x = vstupna hodnota
        (double) n = pocet iteracii

    Vystup:
        (double) logaritmus x
*/
double taylor_log(double x, unsigned int n){

    // kontrola vstupu
    if(isnan(x) || x < 0 || x == (-INFINITY)){
        return NAN;
    }
    else if (x <= 0 ){
        return -INFINITY;
    }
    else if (x == INFINITY){
        return INFINITY;
    }

    // dolezite priradit vstupnu hodnotu
    double aktualna_hodnota = 0; 

    // implementacia pre hodnoty mensie ako 1
    if (x < 1 && x > 0){
        double rozsah = n;
        double mocnina = 1;
        
        // potrebne odratat vstupnu hodnotu od 1
        x = 1 - x; 
        
        for(unsigned int i = 1; i <= rozsah; i++){
            mocnina*=x;
            aktualna_hodnota -= mocnina/i;
        }

        return aktualna_hodnota;
    }

    // implementacia pre hodnoty vacsie alebo rovne 1
    else if (x >= 1){
        double citatel = 1;
        double zlomok_v_citateli = (x-1.0)/x;

        for (unsigned int clen = 1; clen <= n; clen++) {
            citatel*= zlomok_v_citateli;
            aktualna_hodnota += citatel/clen;
        }
        return aktualna_hodnota;
    }

    return -1;
}

/*
Funckia vypocita logaritmus cez zretazene zlomky

    Vstup:
        (double) x = vstupna hodnota
        (double) n = pocet iteracii

    Vystup:
        (double) logaritmus x
*/

double cfrac_log(double x, unsigned int n){

    // kontrola vstupu
    if(isnan(x) || x < 0 || x == (-INFINITY)){
        return NAN;
    }
    else if (x <= 0 ){
        return -INFINITY;
    }
    else if (x == INFINITY){
        return INFINITY;
    }

    // upraveny vzorec na vypocet z
    double z = (x-1)/(x+1);

    // vypocitanie maximalneho fa()
    double a = (n-1.0)*(n-1.0);

    // vypocitanie maximalneho fb()
    double b = (n*2.0)-1;

    double vysledok = 0;
    double z_2 = z*z;

    while (a > 0 && b > 0){
        vysledok = (a*z_2)/(b-vysledok);
        
        // postupne dekremntujem ako fa() tak aj fb()
        b-=2;
        a-=b;
    }

    // posledne upravy pred odovzdanim vysledku
    return (2*z)/(1-vysledok);
}


/*
Funkcia vypocita najnizsi pocet potrebnych iteracii pre cfrac_log(x) a to pomocou binnary search

Podrobny popis fungovania je v prilozenom textovom dokumente

    Vstup:
        (double) x = vstupna hodnota
        (double) log_ref = referencna hodnota logaritmu vypocitana cez log()
        (double) eps = maximalna povolena odchylka
        (unsigned int) max_limit = strop pre binnary search

    Vystup:
        (unsigned int) najnizsi potrebny pocet iteracii
*/
unsigned int cf_iter(double x, double log_ref, double eps, unsigned int max_limit){
    
    unsigned int max = max_limit;
    unsigned int min = 0;
    unsigned int stred = (min+max)/2;

    double rozdiel;

    // testovacie volanie pre overenie vystupu 
    double log_vyp = cfrac_log(x, 1);

    if(isnan(x) || log_vyp == INFINITY || log_vyp == -INFINITY ){
        return 0;
    }

    while(min <= max){
        rozdiel = fabs(cfrac_log(x, stred) - log_ref);

        if(rozdiel <= eps && fabs(cfrac_log(x, stred-1) - log_ref) >= eps){
            return stred;
        }
        else if (rozdiel < eps){
            max = stred-1;
        }
        else{
            min = stred+1;
        }

        stred = (max+min)/2;
    }

    //v pripade chyby nech sa vykona sekvencny vypocet n
    unsigned int n;
    for (n = 1; fabs(cfrac_log(x, n) - log_ref) > eps; n++);
    
    return n; 

}


/*
Funkcia vypocita najnizsi pocet potrebnych iteracii pre taylor_log(x) jednoduchou inkrementaciou poctu iteracii (n)

    Vstup:
        (double) x = vstupna hodnota
        (double) log_ref = referencna hodnota logaritmu vypocitana cez log()
        (double) eps = maximalna povolena odchylka

    Vystup:
        (unsigned int) najnizsi potrebny pocet iteracii
*/
unsigned int taylor_iter(double x, double log_ref, double eps){
    
    // testovacie volanie pre overenie vystupu
    double log_vyp = taylor_log(x, 1);
    if(isnan(x) || log_vyp == INFINITY || log_vyp == -INFINITY ){
        return 0;
    }

    // telo funkcie
    unsigned int n;
    for (n = 1; fabs(taylor_log(x, n) - log_ref) > eps; n++);
    
    return n;
}

/*
Funkcia porovna vstupne hodnoty a vyberie vacsiu

    Vstup: 
        (int) x = hodnota 1
        (int) y = hodnota 2

    Vystup: 
        (int) vysia hodnota
*/
int bigger_value(int x, int y){
    if (x >= y){
        return x;
    }
    else {
        return y;
    }
}

int main(int argc, char const *argv[])
{
    if (argc > 1 && argc <= 5){

        if(strcmp(argv[1], "--log") == 0 && argc == 4){
            char *err_x = NULL;
            char *err_n = NULL;

            double x = strtod(argv[2], &err_x);
            unsigned int n = strtod(argv[3], &err_n);

            // osetrenie nepovolenych znakov
            if(strlen(err_n) != 0 || strlen(err_x) != 0){
                printf("%s\n", ERROR_M);
                return -1;
            }

            // vystup
            printf("Log(%.12g): %.12g\n", x, log(x));
            printf("cf_log(%.12g): %.12g\n", x, cfrac_log(x, n));
            printf("taylor_log(%.12g): %.12g\n", x, taylor_log(x, n));

            // uspesne ukoncenie hlavnej funkcie
            return 0;

        }

        else if(strcmp(argv[1], "--iter") == 0 && argc == 5){
            char *err_min = NULL;
            char *err_max = NULL;
            char *err_eps = NULL;

            double min = strtod(argv[2], &err_min);
            double max = strtod(argv[3], &err_max);
            double eps = strtod(argv[4], &err_eps);
            
            // osetrenie nepovolenych znakov
            if(strlen(err_min) != 0 || strlen(err_max) != 0 || strlen(err_eps) != 0){
                printf("%s\n", ERROR_M);
                return -1;
            }

            // predpocitanie hodnot z dôvodu ze cf_iter() berie za strop minimalny potrebny pocet iteracii od taylor_iter()
            unsigned int taylor_min = taylor_iter(min, log(min), eps);
            unsigned int taylor_max = taylor_iter(max, log(max), eps);
            unsigned int taylor_num_of_iter = bigger_value(taylor_min, taylor_max);

            unsigned int cf_min = cf_iter(min, log(min), eps, taylor_num_of_iter);
            unsigned int cf_max = cf_iter(max, log(max), eps, taylor_num_of_iter);
            unsigned int cf_num_of_iter = bigger_value(cf_min, cf_max);

            // vystup
            printf("log(%.12g) = %.12g\n", min, log(min));
            printf("log(%.12g) = %.12g\n", max, log(max));
            printf("continued fraction iterations = %d\n", cf_num_of_iter);
            printf("cf_log(%.12g) = %.12g\n", min, cfrac_log(min, cf_num_of_iter));
            printf("cf_log(%.12g) = %.12g\n", max, cfrac_log(max, cf_num_of_iter));
            printf("taylor polynomial iterations = %d\n", taylor_num_of_iter);
            printf("taylor_log(%.12g) = %.12g\n", min, taylor_log(min, bigger_value(taylor_min, taylor_num_of_iter)));
            printf("taylor_log(%.12g) = %.12g\n", max, taylor_log(max, bigger_value(taylor_min, taylor_num_of_iter)));
            
            // uspesne ukoncenie hlavnej funkcie
            return 0;
        }
        else{
            // vypis chyboveho hlasenia v pripade nevhodneho poctu parametrov alebo chybneho argumentu
            printf("%s", ERROR_M);
            return -1;
        }
    }
    else{
        printf("%s\n", HELP_M);
        return -1;
    }

}
